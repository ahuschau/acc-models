<h1> {{ scn['label'] }} -  {{ conf['label'] }} optics</h1>

<h2>Twiss functions</h2>
The Twiss functions of this configuration are shown in the interactive plot below. You can zoom in or hover over any curve to obtain more information about the function's value at a specific element. The full TFS table is available [here]({{conf['tfs']}}){target=_blank} and the plot in PDF format [here]({{conf['plot_pdf']}}){target=_blank}. 

<object width="1100" height="{{conf['plot_height']}}" data="{{conf['plot_html']}}"></object> 

<h2>Example scripts</h2>

You can directly open a MAD-X example for this configuration in [SWAN](https://cern.ch/swanserver/cgi-bin/go?projurl=https://gitlab.cern.ch/ahuschau/test-acc-models-ps/raw/master/2019/{{ conf['directory'] }}MADX_example_{{ conf['madx'][:-5] }}.ipynb){target=_blank} or download the necessary files below.

??? "MAD-X example script - [direct download]({{conf['madx']}})"
{{ conf['madx_content'] | indent(8, True) }}

??? "MAD-X strength file - [direct download]({{conf['str']}})"
{{ conf['str_content'] | indent(8, True)}}

??? "MAD-X sequence file - [direct download](../../../{{PSB_seq}})"
{{ PSB_seq_content | indent(8, True)}}

??? "MAD-X strength definition - [direct download](../../../{{PSB_str}})"
{{ PSB_str_content | indent(8, True)}}
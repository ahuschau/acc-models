<h1> {{ scn['label'] }} -  {{ conf['label'] }} optics</h1>

<h2>Twiss functions</h2>
The Twiss functions of this configuration are shown in the interactive plot below. You can zoom in or hover over any curve to obtain more information about the function's value at a specific element. The full TFS table is available [here]({{conf['tfs']}}){target=_blank} and the plot in PDF format [here]({{conf['plot_pdf']}}){target=_blank}. 

<object width="100%" height="{{conf['plot_height']}}" data="{{conf['plot_html']}}"></object> 

{% if ('_HS_' in conf['plot_html']) or ('_PR_' in conf['plot_html']) %}

<h2>Horizontal phase space during the {{ conf['label'].split(' ')[1] }}</h2>

The following plots visualize the creation of the islands and their transportation to large amplitude. The left plot shows the simulated horizontal phase space at the beginning of the lattice and at the indicated time during the cycle. The right plot shows the evolution of the currents of the sextupoles and octupoles used to control the splitting, and the variation of the horizontal tune to control the distance of the islands from the origin. The vertical dashed line indicates the currently selected cycle time.

<object width="1100" height="500" data="{{conf['plot_html'][:-5] + '_2.html'}}"></object> 

{% endif %}

<h2>Example scripts</h2>

You can directly open a MAD-X example for this configuration in [SWAN](https://cern.ch/swanserver/cgi-bin/go?projurl=https://gitlab.cern.ch/ahuschau/test-acc-models-ps/raw/master/2019/{{ conf['directory'] }}MADX_example_{{ conf['madx'][:-5] }}.ipynb){target=_blank} or download the necessary files below.

??? "MAD-X example script - [direct download]({{conf['madx']}})"
{{ conf['madx_content'] | indent(8, True) }}

??? "MAD-X strength file - [direct download]({{conf['str']}})"
{{ conf['str_content'] | indent(8, True)}}

??? "MAD-X sequence of straight section elements - [direct download](../../../{{PS_SS_seq}})"
{{ PS_SS_content | indent(8, True)}}

??? "MAD-X sequence of main units - [direct download](../../../{{PS_MU_seq}})"
{{ PS_MU_content | indent(8, True)}}

??? "MAD-X strength definition - [direct download](../../../{{PS_str}})"
{{ PS_str_content | indent(8, True)}}

<!---
[![MAD-X example file](../../../../../_images/icons/madx.png)]({{conf['madx']}})
[![Strength file](../../../../../_images/icons/str.png)]({{conf['str']}})
[![Sequence of straight section elements](../../../../../_images/icons/SS.png)](../../../{{PS_SS_seq}})
[![Sequence of straight main units](../../../../../_images/icons/MU.png)](../../../{{PS_MU_seq}})
--->